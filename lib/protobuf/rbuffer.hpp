#ifndef RBUFFER_HPP
#define RBUFFER_HPP

#include <ReadBufferInterface.h>
#include <cstdlib>
#include <etl/deque.h>

template <const size_t MAX_SIZE>
class rbuffer : public ::EmbeddedProto::ReadBufferInterface
{
public:
  rbuffer();

  //! Obtain the total number of bytes currently stored in the buffer.
  uint32_t
  get_size() const;

  //! Obtain the total number of bytes which can at most be stored in the
  //! buffer.
  uint32_t
  get_max_size() const;

  //! Obtain the value of the oldest byte in the buffer.
  /*!
   This function will not alter the buffer read index.

   The parameter byte will not be set if the buffer was empty.

   \param[out] byte When the buffer is not empty this variable will hold the
   oldest value. \return True when the buffer was not empty.
   */
  bool
  peek(uint8_t &byte) const;

  //! Advances the internal read index by one when the buffer is not empty.
  void
  advance();

  //! Advances the internal read index by the given value.
  /*!
   The advance is limited to the number of bytes in the buffer.
   \param[in] n_bytes The number of bytes to advance the read index.
   */
  void
  advance(const uint32_t n_bytes);

  //! Obtain the value of the oldest byte in the buffer and remove it from the
  //! buffer.
  /*!
   This function will alter the internal read index.

   The parameter byte will not be set if the buffer was empty.

   \param[out] byte When the buffer is not empty this variable will hold the
   oldest value. \return True when the buffer was not empty.
   */
  bool
  pop(uint8_t &byte);

  void
  push(uint8_t b);

private:
  etl::deque<uint8_t, MAX_SIZE> m_q;
};

#endif /* RBUFFER_HPP */
