#include <rbuffer.hpp>

template <const size_t MAX_SIZE>
rbuffer<MAX_SIZE>::rbuffer() :
  ReadBufferInterface()
{
}

template <const size_t MAX_SIZE>
uint32_t
rbuffer<MAX_SIZE>::get_size() const
{
  return m_q.size();
}

template <const size_t MAX_SIZE>
uint32_t
rbuffer<MAX_SIZE>::get_max_size() const
{
  return MAX_SIZE;
}

template <const size_t MAX_SIZE>
bool
rbuffer<MAX_SIZE>::peek(uint8_t &byte) const
{
  if (m_q.empty()) {
    return false;
  }
  byte = m_q.front();
  return true;
}

template <const size_t MAX_SIZE>
void
rbuffer<MAX_SIZE>::advance()
{
  // NOOP
}

template <const size_t MAX_SIZE>
void
rbuffer<MAX_SIZE>::advance(const uint32_t n_bytes)
{
  // NOOP
}

template <const size_t MAX_SIZE>
bool
rbuffer<MAX_SIZE>::pop(uint8_t &byte)
{
  if (m_q.empty()) {
    return false;
  }
  byte = m_q.front();
  m_q.pop_front();
  return true;
}

template <const size_t MAX_SIZE>
void
rbuffer<MAX_SIZE>::push(uint8_t b)
{
  m_q.push_back(b);
}
