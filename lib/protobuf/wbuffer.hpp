#ifndef WBUFFER_HPP
#define WBUFFER_HPP

#include <WriteBufferInterface.h>
#include <cstdlib>
#include <etl/deque.h>

template <const size_t MAX_SIZE>
class wbuffer : public ::EmbeddedProto::WriteBufferInterface
{
public:
  wbuffer();

  //! Delete all data in the buffer.
  void
  clear();

  //! Obtain the total number of bytes currently stored in the buffer.
  uint32_t
  get_size() const;

  //! Obtain the total number of bytes which can at most be stored in the
  //! buffer.
  uint32_t
  get_max_size() const;

  //! Obtain the total number of bytes still available in the buffer.
  uint32_t
  get_available_size() const;

  //! Push a single byte into the buffer.
  /*!
   \param[in] byte The data to append after previously added data in the buffer.
   \return True when there was space to add the byte.
   */
  bool
  push(const uint8_t byte);

  //! Push an array of bytes into the buffer.
  /*!
   The given array will be appended after already added data in the buffer.
   \param[in] bytes Pointer to the array of bytes.
   \param[in] length The number of bytes in the array.
   \return True when there was space to add the bytes.
   */
  bool
  push(const uint8_t *bytes, const uint32_t length);

  uint8_t
  operator[](size_t index) const;

private:
  etl::deque<uint8_t, MAX_SIZE> m_q;
};

#endif /* WBUFFER_HPP */
