#include <wbuffer.hpp>

template <const size_t MAX_SIZE>
wbuffer<MAX_SIZE>::wbuffer() :
  WriteBufferInterface()
{
}

//! Delete all data in the buffer.
template <const size_t MAX_SIZE>
void
wbuffer<MAX_SIZE>::clear()
{
  m_q.clear();
}

//! Obtain the total number of bytes currently stored in the buffer.
template <const size_t MAX_SIZE>
uint32_t
wbuffer<MAX_SIZE>::get_size() const
{
  return m_q.size();
}

//! Obtain the total number of bytes which can at most be stored in the
//! buffer.
template <const size_t MAX_SIZE>
uint32_t
wbuffer<MAX_SIZE>::get_max_size() const
{
  return MAX_SIZE;
}

//! Obtain the total number of bytes still available in the buffer.
template <const size_t MAX_SIZE>
uint32_t
wbuffer<MAX_SIZE>::get_available_size() const
{
  return m_q.available();
}

//! Push a single byte into the buffer.
/*!
\param[in] byte The data to append after previously added data in the buffer.
\return True when there was space to add the byte.
*/
template <const size_t MAX_SIZE>
bool
wbuffer<MAX_SIZE>::push(const uint8_t byte)
{
  if (m_q.available() > 0) {
    m_q.push_back(byte);
    return true;
  }
  return false;
}

//! Push an array of bytes into the buffer.
/*!
The given array will be appended after already added data in the buffer.
\param[in] bytes Pointer to the array of bytes.
\param[in] length The number of bytes in the array.
\return True when there was space to add the bytes.
*/
template <const size_t MAX_SIZE>
bool
wbuffer<MAX_SIZE>::push(const uint8_t *bytes, const uint32_t length)
{
  if (m_q.available() < length) {
    return false;
  }
  for (uint32_t i = 0; i < length; i++) {
    m_q.push_back(bytes[i]);
  }
  return true;
}

template <const size_t MAX_SIZE>
uint8_t
wbuffer<MAX_SIZE>::operator[](size_t index) const
{
  return m_q[index];
}
