#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <MQTT.h>
#include <IotWebConf.h>

#define AP_NAME "satnogs-tiny"
#define INITIAL_AP_PASS ""
#define CONFIG_VERSION "init"

#define MQTT_DEFAULT_SERVER "broker.hivemq.com" // This is a public broker for testing only
#define MQTT_DEFAULT_PORT "1883"
#define MQTT_DEFAULT_TOPIC "lsf/dev"

#define MAX_STRING_LEN 128
#define MAX_NUMBER_LEN 32

/*
 * A singleton class that holds all the configuration parameters
 */
class config : public IotWebConf {
public:
  /*
   *
   */
  static config& instance() {
    static config holder;
    return holder;
  }

  String
  get_mqtt_server() const;

  int
  get_mqtt_port() const;

  String
  get_mqtt_username() const;

  String
  get_mqtt_pass() const;

  int
  get_station_id() const;

  float
  get_station_lat() const;

  float
  get_station_lon() const;

  void
  mqtt_clear_credentials();

  void
  restart_if_needed();

private:
  config();

  void
  handle_root();

  void
  config_saved();

  void
  config_reset();

  bool
  form_validator(iotwebconf::WebRequestWrapper* webRequestWrapper);

  DNSServer dns_server;
  WebServer web_server;
  bool need_reset = false;
  
  char station_id[MAX_NUMBER_LEN];
  char api_key[MAX_STRING_LEN];
  char lat[MAX_NUMBER_LEN];
  char lon[MAX_NUMBER_LEN];

  char mqtt_server[MAX_STRING_LEN];
  char mqtt_port[MAX_NUMBER_LEN];
  char mqtt_username[MAX_STRING_LEN];
  char mqtt_pass[MAX_STRING_LEN];

  iotwebconf::ParameterGroup station_group = iotwebconf::ParameterGroup("station", "Ground Station configuration");
  iotwebconf::NumberParameter station_id_param = iotwebconf::NumberParameter("Station ID", "station_id", station_id, MAX_NUMBER_LEN, nullptr, nullptr, "required min='0' step='1'");
  iotwebconf::TextParameter api_key_param = iotwebconf::TextParameter("API Key", "api_key", api_key, MAX_STRING_LEN, nullptr, nullptr, "required type=\"text\"");
  iotwebconf::NumberParameter lat_param = iotwebconf::NumberParameter("Latitude (3 decimals, will be public)", "lat", lat, MAX_NUMBER_LEN, NULL, "0.000", "required min='-180' max='180' step='0.001'");
  iotwebconf::NumberParameter lon_param = iotwebconf::NumberParameter("Longitude (3 decimals, will be public)", "lng", lon, MAX_NUMBER_LEN, NULL, "-0.000", "required min='-180' max='180' step='0.001'");

  iotwebconf::ParameterGroup mqtt_group = iotwebconf::ParameterGroup("mqtt", "MQTT configuration");
  iotwebconf::TextParameter mqtt_server_param = iotwebconf::TextParameter("MQTT server", "mqtt_server", mqtt_server, MAX_STRING_LEN, MQTT_DEFAULT_SERVER, MQTT_DEFAULT_SERVER, "required type='text' minlength='3'");
  iotwebconf::NumberParameter mqtt_port_param = iotwebconf::NumberParameter("MQTT port", "mqtt_port", mqtt_port, MAX_NUMBER_LEN, MQTT_DEFAULT_PORT, MQTT_DEFAULT_PORT, "required min='0000' max='9999' step='1'");
  iotwebconf::TextParameter mqtt_user_name_param = iotwebconf::TextParameter("MQTT user", "mqtt_username", mqtt_username, MAX_STRING_LEN);
  iotwebconf::PasswordParameter mqtt_pass_param = iotwebconf::PasswordParameter("MQTT password", "mqtt_pass", mqtt_pass, MAX_STRING_LEN);
};

#endif // CONFIG_HPP
