#include <Arduino.h>
#include <config.hpp>

WiFiClient net;
MQTTClient mqtt_client;
bool need_mqtt_connect = false;

config &cnf = config::instance();

void
wifi_connected();

void
mqtt_init(bool valid_config);

bool
connect_mqtt();

bool
connect_mqtt_options();

void
mqtt_msg_received(String &topic, String &payload);

void setup() {
  Serial.begin(115200);
  cnf.setWifiConnectionCallback(&wifi_connected);
  bool valid_config = cnf.init();
  mqtt_init(valid_config);
}

void loop() {
  cnf.doLoop();
  mqtt_client.loop();
  
  if (need_mqtt_connect) {
    if (connect_mqtt()) {
      need_mqtt_connect = false;
    }
  }
  else if ((cnf.getState() == iotwebconf::OnLine) && (!mqtt_client.connected())) {
    Serial.println("MQTT reconnect");
    connect_mqtt();
  }

  cnf.restart_if_needed();
}

void
wifi_connected()
{
  need_mqtt_connect = true;
}

void
mqtt_init(bool valid_config)
{
  if (!valid_config) {
    cnf.mqtt_clear_credentials();
  }
  mqtt_client.begin(cnf.get_mqtt_server().c_str(), cnf.get_mqtt_port(), net);
  mqtt_client.onMessage(&mqtt_msg_received);
}

bool
connect_mqtt()
{
  Serial.print("Connecting to MQTT server... ");
  while (!connect_mqtt_options()) {
    delay(1000);
  }
  Serial.println("Success!");

  String station_topic = String(MQTT_DEFAULT_TOPIC "/station/") + String(cnf.get_station_id());
  Serial.printf("Subscribe to '%s' topic\n", station_topic.c_str());
  mqtt_client.subscribe(station_topic);

  return true;
}

bool
connect_mqtt_options()
{
  bool result;
  if (cnf.get_mqtt_pass().c_str()[0] != '\0') {
    result = mqtt_client.connect(cnf.getThingName(), cnf.get_mqtt_username().c_str(), cnf.get_mqtt_pass().c_str());
  }
  else if (cnf.get_mqtt_username().c_str()[0] != '\0') {
    result = mqtt_client.connect(cnf.getThingName(), cnf.get_mqtt_username().c_str());
  }
  else {
    result = mqtt_client.connect(cnf.getThingName());
  }

  return result;
}

void
mqtt_msg_received(String &topic, String &payload)
{
  Serial.println("Incoming: " + topic + " - " + payload);
}